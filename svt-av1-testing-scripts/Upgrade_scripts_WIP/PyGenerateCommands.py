import os
import sys
import re
import glob
import stat
import subprocess
import binascii
import multiprocessing
from datetime import datetime
import shutil
import hashlib
import ast
import time

'''
SPIE Intraperiod Settings

SPIE2020 8bit : 128
SPIE2020 10bit : 119
SPIT2021 : -1
'''

# Changeable Settings Start

'''Add special parameters to commands for testing (i.e. --pred-struct 1, --tune 0)'''
INSERT_SPECIAL_PARAMETERS = []

TEST_SETTINGS = {
    ##'''Test Parameters'''##
    'test_config': 'SPIE2021_svt_for_testing',
    'stream_dir': r'/user/stream',
    'presets': [13],
    'intraperiod': -1,
    'decode_cycles': 1,

    ##'''System Parameters'''##
    'decode_threads': 3,
    'decode_jobs': 20,
    'num_pool': 96,
    'ffmpeg_job_count': 20,
    'vmaf_job_count': 96,
}

# Changeable Settings End




def test_configurations():
    TEST_REPOSITORY = {
        'SPIE2021_svt'       : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_svt_CRF_1lp_1p']],
        'SPIE2021_aom'       : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],
        'SPIE2021_x264'      : [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_x264_CRF_1p']],
        'SPIE2021_x265'      : [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_x265_CRF_1p']],
        'SPIE2021_vp9'       : [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'],   DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_vp9_CRF_2p']],


        'SPIE2020_8bit_svt'  : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'SPIE2020_8bit_aom'  : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_aom_CRF_2p']],
        'SPIE2020_8bit_x264' : [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_8bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x264_CRF_1p']],
        'SPIE2020_8bit_x265' : [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_8bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x265_CRF_1p']],
        'SPIE2020_8bit_vp9'  : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_8bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_vp9_CRF_2p']],


        'SPIE2020_10bit_svt' : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'SPIE2020_10bit_aom' : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_aom_CRF_2p']],
        'SPIE2020_10bit_x264': [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_10bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x264_CRF_1p']],
        'SPIE2020_10bit_x265': [RC_VALUES['SPIE2020_x264_x265'], RESOLUTION['SPIE2020_10bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_x265_CRF_1p']],
        'SPIE2020_10bit_vp9' : [RC_VALUES['SPIE2020_svt_aom'], RESOLUTION['SPIE2020_10bit'],   DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2020_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2020_vp9_CRF_2p']],

        'ELFUENTE_ffmpeg_svt_fast_decode': [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['ffmpeg_svt_fast_decode']],

        'NETFLIX_svt': [RC_VALUES['netflix_crf'], RESOLUTION['netflix'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['netflix_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p']],
        'NETFLIX_libaom': [RC_VALUES['netflix_crf'], RESOLUTION['netflix'], DOWNSCALE_COMMAND['SPIE2020_scaling'], METRIC_COMMAND['netflix_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['SPIE2021_aom_CRF_2p']],

        'SPIE2021_svt_for_testing': [RC_VALUES['fast_testing_qps'], RESOLUTION['fast_testing_resolutions'], DOWNSCALE_COMMAND['SPIE2021_scaling'], METRIC_COMMAND['SPIE2021_ffmpeg_rescale'], ENCODE_COMMAND['SPIE2021_svt_CRF_1lp_1p']],

    }

    return TEST_REPOSITORY


def main():
    t0 = time.time()
    check_if_latest_version()

    if not os.path.isdir(TEST_SETTINGS['stream_dir']):
        print('[ERROR] The stream folder specified does not exist. Exiting...')
        sys.exit()
    if TEST_SETTINGS['test_config'] not in test_configurations():
        print('[ERROR] Invalid test configuration specified. Exiting...')
        sys.exit()
        
    run_unit_tests = 0

    '''SPIE2021 Unit Test'''
    unit_test = ['SPIE2021_svt', 'SPIE2021_aom', 'SPIE2021_x264', 'SPIE2021_x265', 'SPIE2021_vp9']

    '''SPIE2020 10bit Unit Test'''
    unit_test = ['SPIE2020_10bit_svt', 'SPIE2020_10bit_aom', 'SPIE2020_10bit_x264', 'SPIE2020_10bit_x265', 'SPIE2020_10bit_vp9']

    '''SPIE2020 8bit Unit Test'''
    unit_test = ['SPIE2020_8bit_svt', 'SPIE2020_8bit_aom', 'SPIE2020_8bit_x264', 'SPIE2020_8bit_x265', 'SPIE2020_8bit_vp9']

    check_script_integrity()

    if run_unit_tests:
        test_script(unit_test)
    elif TEST_SETTINGS['test_config']:
        feature_to_test = TEST_SETTINGS['test_config']
    else:
        print('No test Config Specified. Exiting...')
        sys.exit()

    if not run_unit_tests:
        generate_test(feature_to_test)

    print('Test Generation Took %s Seconds' % (round(time.time() - t0, 3)))


def generate_test(test_name, verify_commands=False):
    TEST_REPOSITORY = test_configurations()

    '''Local bitstreams folder'''
    source_dir = os.path.join(os.getcwd(), 'bitstreams')

    '''Pull test info from Test repo'''
    rc_values = TEST_REPOSITORY[test_name][0]
    resolutions = TEST_REPOSITORY[test_name][1]
    downscale_command_template = TEST_REPOSITORY[test_name][2]
    metric_command_template = TEST_REPOSITORY[test_name][3]
    encoding_command_template = TEST_REPOSITORY[test_name][4]

    '''Get metric command dict name for file naming purposes'''
    metric_id = list(METRIC_COMMAND.keys())[list(METRIC_COMMAND.values()).index(metric_command_template)]

    '''Insert a few extra tokens for the case where we don't want to make a while new test configuration'''
    if INSERT_SPECIAL_PARAMETERS and not verify_commands:
        encoding_command_template = insert_new_parameters(encoding_command_template)

    '''Generate commands for test being performed'''
    encoding_commands, metric_commands, copy_commands, downscale_commands, decode_commands = process_command_template(
        encoding_command_template, metric_command_template, downscale_command_template, rc_values, resolutions, verify_commands)

    '''Write the important parameters to a log to act as a double checking mechanism'''
    if not verify_commands:
        write_parameters(test_name, rc_values, resolutions, downscale_command_template, encoding_command_template, metric_command_template, INSERT_SPECIAL_PARAMETERS)

    '''Define command file names according to their configuration name'''
    encode_file_id = 'run-{}'.format(test_name)
    metric_file_id = 'run-{}'.format(metric_id)

    '''Either return the commands to the integrity check for validation or write commands to file'''
    if verify_commands:
        return encoding_commands, metric_commands, copy_commands, downscale_commands, decode_commands
    else:
        encode_file_id, metric_file_id = write_commands_to_files(encoding_commands, metric_commands, copy_commands, downscale_commands, decode_commands, encode_file_id, metric_file_id)
        generate_bash_driver_file(encoding_command_template, encode_file_id, metric_file_id, metric_id, resolutions)


'''Core Sample Command processing functions'''


def process_command_template(encoding_command_template, metric_command_template, downscale_command_template, rc_values, resolutions, verify_commands):
    encode_commands = list()
    metric_commands = list()
    decode_commands = list()
    copy_commands, downscale_commands = ('', '')

    for preset in TEST_SETTINGS['presets']:
        if 'aomenc' in encoding_command_template and preset > 6:
            preset = 6
# if './x265' in encoding_command_template or './x264' in encoding_command_template and preset == 13:
##            preset = 0

        vvenc_presets = ["slow", "medium", "fast", "faster"]

        per_preset_encode_commands = list()
        per_preset_metric_commands = list()
        per_preset_decode_commands = list()

        if verify_commands:
            clip_lists = [['test.y4m']]
        else:
            clip_lists = sort_clip_list_by_complexity(TEST_SETTINGS['stream_dir'])

        if resolutions:
            copy_commands, downscale_commands, clip_lists, parameter_tracker = get_downscaled_clip_list(clip_lists, resolutions, downscale_command_template, verify_commands)

        for i in range(len(clip_lists)):
            for rc_value in rc_values:
                for clip in clip_lists[i]:
                    '''Retrieve the relevant parameters for the given clip'''
                    if clip.endswith('yuv') and resolutions:
                        width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = parameter_tracker[clip]
                    elif clip.endswith('y4m') and resolutions:
                        width, height, framerate, number_of_frames = parameter_tracker[clip]
                    elif clip.endswith('yuv') and yuv_library_found:
                        width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
                    elif clip.endswith('y4m') and ('SvtAv1EncApp' not in encoding_command_template and TEST_SETTINGS['intraperiod'] == -1):
                        width, height, framerate, number_of_frames = read_y4m_header(clip)
                    else:
                        print('[Warning]: Skipping Clip: {}'.format(clip))
                        continue

                    '''Set the intraperiod to +1 number of frames for encoders that do not support -1 keyint'''
                    if 'SvtAv1EncApp' not in encoding_command_template and TEST_SETTINGS['intraperiod'] == -1:
                        intraperiod = number_of_frames + 1
                    else:
                        intraperiod = TEST_SETTINGS['intraperiod']

                    '''Set the pixfmt according to the bitdepth'''
                    if clip.endswith('yuv'):
                        if bitdepth == 10:
                            pixfmt = "yuv420p10le"
                            vvenc_pixfmt = 'yuv420_10'
                        else:
                            pixfmt = "yuv420p"
                            vvenc_pixfmt = 'yuv420'

                    '''Assign the rc identifier based on the number of digits'''
                    if len(str(rc_value)) > 2:
                        rc_string = 'TBR{}kbps'.format(rc_value)
                    else:
                        rc_string = 'Q{}'.format(rc_value)

                    '''Set the vvenc presets according to the preset number'''
                    if preset < 4:
                        vvenc_preset = vvenc_presets[preset]
                    else:
                        vvenc_preset = "faster"

                    '''Get reference clip resolution for the rescale case'''
                    ref_res = re.search(r'(\d+x\d+)to(\d+x\d+)', clip)

                    if ref_res:
                        ref_width = ref_res.group(1).split('x')[0]
                        ref_height = ref_res.group(1).split('x')[1]
                        mod_width = ref_res.group(2).split('x')[0]
                        mod_height = ref_res.group(2).split('x')[1]
                        ref_clip = re.sub('to\\d+x\\d+', '', clip)
                    else:
                        ref_width = mod_width = width
                        ref_height = mod_height = height
                        ref_clip = clip

                    '''Generic setup of parameters and conditionals'''
                    clip_name = os.path.split(clip)[1]
                    rawvideo = 'rawvideo'
                    vmaf_pixfmt = '420'

                    output_filename = '{}/{}_M{}_{}_{}'.format('bitstreams', TEST_SETTINGS['test_config'], preset, clip_name[:-4], rc_string)

                    '''remove yuv tokens in case of y4m clip'''
                    if clip.endswith('y4m'):
                        encode_command = remove_yuv_tokens(encoding_command_template)
                        metric_command = remove_yuv_tokens(metric_command_template)
                    elif clip.endswith('yuv'):
                        encode_command = encoding_command_template
                        metric_command = metric_command_template

                    '''sub in the values'''
                    temp_clip = '/dev/shm/{}.{}'.format(output_filename, clip[-4:])
                    '''Generate Encode Commands'''
                    encode_command = encode_command.format(**vars())
                    '''Generate Metric Command'''
                    metric_command = metric_command.format(**vars())

                    if TEST_SETTINGS['decode_cycles']:
                        decode_command = generate_decode_commands(output_filename)
                        per_preset_decode_commands.append(decode_command)

                    per_preset_encode_commands.append(encode_command)
                    per_preset_metric_commands.append(metric_command)

        encode_commands.append(per_preset_encode_commands)
        metric_commands.append(per_preset_metric_commands)
        decode_commands.append(per_preset_decode_commands)

    return encode_commands, metric_commands, copy_commands, downscale_commands, decode_commands


def get_downscaled_clip_list(clip_lists, downscale_target_resolutions, downscale_command_template, verify_commands):
    downscaled_clip_map = dict()
    parameter_tracker = dict()
    downscaled_clip_list = list()
    copy_commands = list()
    downscale_commands = list()
    rawvideo = 'rawvideo'
    fps = '30000/1001'
    resolution_id = list(RESOLUTION.keys())[list(RESOLUTION.values()).index(downscale_target_resolutions)]

    for i in range(len(clip_lists)):
        for clip in clip_lists[i]:
            if verify_commands:
                width, height, framerate, number_of_frames = ['1920', '1080', 30, 60]
            elif clip.endswith('yuv') and yuv_library_found:
                width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
            elif clip.endswith('y4m'):
                width, height, framerate, number_of_frames = read_y4m_header(clip)
            else:
                print('[Warning]: Skipping Clip: {}'.format(clip))
                continue

            '''Assign the pixel format variable based on bitdepth'''
            if clip.endswith('yuv'):
                if bitdepth == 10:
                    pixfmt = "yuv420p10le"
                else:
                    pixfmt = "yuv420p"

            '''Netflix has special test case where we change the target resolutions depending on the original source resolution'''
            if 'netflix' in resolution_id:
                if height == 2160:
                    downscale_target_resolutions = [(2560, 1440), (1920, 1080), (1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216)]  # 8bit
                else:
                    downscale_target_resolutions = [(2560, 1088), (1920, 816), (1280, 544), (960, 408), (748, 318), (588, 250), (480, 204), (372, 158)]  # 8bit

            '''remove yuv tokens in case of y4m clip'''
            if clip.endswith('y4m'):
                cleaned_downscale_command_template = remove_yuv_tokens(downscale_command_template)
            else:
                cleaned_downscale_command_template = downscale_command_template

            '''Assign target path for source and scaled clip'''
            resized_clip = os.path.join('resized_clips', os.path.split(clip)[-1])

            '''Remove widthxheight occurences from clip name to avoid detection issues'''
            resized_clip = re.sub(r'\d+x\d+', '', resized_clip).replace('__', '_')

            '''Append source resolution to end of clip'''
            ref_clip = resized_clip[:-4] + '_{width}x{height}'.format(**vars()) + resized_clip[-4:]

            '''Check if resolution is in dict'''
            if "{width}x{height}".format(**vars()) not in downscaled_clip_map:
                downscaled_clip_map["{width}x{height}".format(**vars())] = [ref_clip]
            else:
                downscaled_clip_map["{width}x{height}".format(**vars())].append(ref_clip)

            '''Create copy command'''
            copy_command = "cp {clip} {ref_clip}".format(**vars())
            copy_commands.append(copy_command)

            for target_width, target_height in downscale_target_resolutions:
                if int(target_width) >= int(width) and int(target_height) >= int(height):
                    continue

                '''Name of output downscaled clip'''
                scaled_clip_name = resized_clip[:-4] + '_{width}x{height}to{target_width}x{target_height}'.format(**vars()) + resized_clip[-4:]

                if "{target_width}x{target_height}".format(**vars()) not in downscaled_clip_map:
                    downscaled_clip_map["{target_width}x{target_height}".format(**vars())] = [scaled_clip_name]
                else:
                    downscaled_clip_map["{target_width}x{target_height}".format(**vars())].append(scaled_clip_name)

                '''Fill in downscale template'''
                downscale_command = cleaned_downscale_command_template.format(**vars())

                '''Keep track of parameters of downscaled clips'''
                if resized_clip.endswith('yuv'):
                    parameter_tracker[scaled_clip_name] = [width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                    parameter_tracker[ref_clip] = [width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                if resized_clip.endswith('y4m'):
                    parameter_tracker[scaled_clip_name] = [width, height, framerate, number_of_frames]
                    parameter_tracker[ref_clip] = [width, height, framerate, number_of_frames]

                downscale_commands.append(downscale_command)

    key_list = sorted(downscaled_clip_map.keys(), key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)

    for target_res in key_list:
        downscaled_clip_list.append(downscaled_clip_map[target_res])

    return copy_commands, downscale_commands, downscaled_clip_list, parameter_tracker


def generate_bash_driver_file(encoding_command_template, encode_file_id, metric_file_id, metric_id, resolutions):
    encoder_executable_found = re.search(r'\s*\.\/(.*?)\s', encoding_command_template)

    if encoder_executable_found:
        encoder_exec = encoder_executable_found.group(1)
    else:
        print("Warning encoder executable not specified in bash script")

    run_all_paral_file_name = 'run-{}-all-paral.sh'.format(encoder_exec)
    run_paral_cpu_file_name = 'run-{}-paral-cpu.sh'.format(encoder_exec)

    with open(run_paral_cpu_file_name, 'w') as file:
        file.write("parallel -j {} < {}-m$1.txt".format(TEST_SETTINGS['num_pool'], encode_file_id))

    run_all_paral_script = []
    run_all_paral_script.append("#!/bin/bash")
    run_all_paral_script.append("encoder_type=\"{}\"".format(encoder_exec))
    run_all_paral_script.append("enc_mode_array=({})".format(' '.join(map(str, TEST_SETTINGS['presets']))))
    run_all_paral_script.append("debug_date=" + r"`date " + "\"+%Y-%m-%d_%H%M%S\"`")
    run_all_paral_script.append("debug_filename=" + "\"debug_output_automation_${debug_date}.txt\"")
    run_all_paral_script.append("mkdir {}".format('bitstreams'))
    run_all_paral_script.append("chmod +rx tools")
    run_all_paral_script.append("chmod +rx tools/*")
    run_all_paral_script.append("chmod +rx *.sh")
    run_all_paral_script.append("chmod +x {}".format(encoder_exec))

    if encoder_exec == 'vvencapp':
        run_all_paral_script.append("chmod +x vvdecapp")
    if 'vmaf_exe' in metric_id:
        run_all_paral_script.append('mkdir /dev/shm/bitstreams')

# if encoder == 'ffmpeg-libaom' and passes==2:
##        run_all_paral_script.append("mkdir 2pass_logs")

    if TEST_SETTINGS['decode_cycles']:
        run_all_paral_script.append("mkdir {}".format('decode_log_bitstreams/'))

    if resolutions:
        run_all_paral_script.append("mkdir resized_clips")
        run_all_paral_script.append("parallel -j {} < run_copy_reference.txt".format(TEST_SETTINGS['ffmpeg_job_count']))
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j {} < run_downscale.txt) &> time_downscale.log &".format(TEST_SETTINGS['ffmpeg_job_count']))
        run_all_paral_script.append("wait && echo \'Downscaling Finished\'")

    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    run_all_paral_script.append("\tif [ \"$encoder_type\" == \"aomenc\" ] || [ \"$encoder_type\" == \"vpxenc\" ]; then")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) > time_enc_$i.log 2>&1 &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\telse")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) &> time_enc_$i.log &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\tfi\n")
    run_all_paral_script.append("\twait $b && echo \'Encoding Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")

    if 'vmaf_exe' in metric_id:
        metric_jobs = TEST_SETTINGS['vmaf_job_count']
    elif 'SPIE2021' in metric_id:
        metric_jobs = TEST_SETTINGS['vmaf_job_count']
    else:
        metric_jobs = TEST_SETTINGS['ffmpeg_job_count']

    run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(metric_jobs, metric_file_id))
    run_all_paral_script.append("\twait $b && echo \'Running metric commands for Encode Mode \'$i\' Finished\' >> ${debug_filename}")

    run_all_paral_script.append("done")
    if TEST_SETTINGS['decode_cycles']:
        run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
        run_all_paral_script.append("\techo \'Running M\'$i")
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-ffmpeg-decode-m$i.txt) &> time_decode_$i.log &".format(TEST_SETTINGS['decode_jobs'], encode_file_id))
        run_all_paral_script.append("\twait $b && echo \'Running ffmpeg decode for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
        run_all_paral_script.append("done")

    run_all_paral_script.append("python PyCollectResults.py")

    with open(run_all_paral_file_name, 'w') as file:
        for line in run_all_paral_script:
            file.write(line + '\n')

    # chmoding bash files
    # NOTE: os.chmod is finicky -> may not work
    file_stat = os.stat(run_all_paral_file_name)
    os.chmod(run_all_paral_file_name, file_stat.st_mode | stat.S_IEXEC)

    file_stat = os.stat(run_paral_cpu_file_name)
    os.chmod(run_paral_cpu_file_name, file_stat.st_mode | stat.S_IEXEC)


def remove_yuv_tokens(command_template):
    command_tokens = re.findall('\\s*\\S*\\s*{.*?}', command_template)

    for command_token in command_tokens:
        command_param = command_token.split('{')[-1].strip('}')

        if command_param in YUV_PARAMS:
            command_template = command_template.replace(command_token, '')

    return command_template


def write_parameters(test_name, rc_values, resolutions, downscale_command_template, encoding_command_template, metric_command_template, INSERT_SPECIAL_PARAMETERS):
    file_name = '{}-parameters-{}.txt'.format(test_name, datetime.now().strftime("%Y-%m-%d_H%H-M%M-S%S"))
    with open(file_name, 'w') as file:
        file.write('test_name: {test_name}\n'.format(**vars()))
        file.write('rc_values: {rc_values}\n'.format(**vars()))
        file.write('resolutions: {resolutions}\n'.format(**vars()))
        file.write('downscale_command_template: {downscale_command_template}\n'.format(**vars()))
        file.write('encoding_command_template: {encoding_command_template}\n'.format(**vars()))
        file.write('metric_command_template: {metric_command_template}\n'.format(**vars()))
        file.write('INSERT_SPECIAL_PARAMETERS: {INSERT_SPECIAL_PARAMETERS}\n'.format(**vars()))


def write_commands_to_files(encoding_commands, metric_commands, copy_commands, downscale_commands, decode_commands, encode_file_id, metric_file_id):  # Good
    '''Write Encode and metric Commands'''
    for preset, encode_commands, metric_commands in zip(TEST_SETTINGS['presets'], encoding_commands, metric_commands):
        with open('{}-m{}.txt'.format(encode_file_id, preset), 'w') as encode_file,\
                open('{}-m{}.txt'.format(metric_file_id, preset), 'w') as metric_file:
            for command in encode_commands:
                encode_file.write('{}\n'.format(command))
            for command in metric_commands:
                metric_file.write('{}\n'.format(command))

    '''Write Decode commands'''
    for preset, decode_commands in zip(TEST_SETTINGS['presets'], decode_commands):
        with open('{}-ffmpeg-decode-m{}.txt'.format(encode_file_id, preset), 'w') as decode_file:
            for command in decode_commands:
                decode_file.write('{}\n'.format(command))

    '''Write Scaling commands'''
    with open('run_copy_reference.txt', 'w') as copy_file,\
            open('run_downscale.txt', 'w') as downscale_file:
        for command in copy_commands:
            copy_file.write('{}\n'.format(command))
        for command in downscale_commands:
            downscale_file.write('{}\n'.format(command))

    return encode_file_id, metric_file_id


def insert_new_parameters(token):  # Good
    sub_string_first_half = token.rpartition('-i')[0]
    sub_string_second_half = token.rpartition('-i')[2]

    for param in INSERT_SPECIAL_PARAMETERS:
        sub_string_first_half += param + " "

    sample_command = sub_string_first_half + "-i" + sub_string_second_half
    return sample_command


def generate_decode_commands(input_bin):  # Good
    decode_command_template = '(/usr/bin/time --verbose tools/ffmpeg -threads {decode_threads} -i {input_bin}.bin  -f null - ) > {output_decode_log}.log 2>&1'''
    decode_path = 'decode_log_bitstreams'
    decode_threads = TEST_SETTINGS['decode_threads']

    stream_name = os.path.split(input_bin)[-1]
    output_decode_log = os.path.join(decode_path, stream_name)
    decode_command = decode_command_template.format(**vars())

    return decode_command


'''Two functions to extract clip info from its y4m header'''


def read_y4m_header_helper(readByte, buffer):
    if sys.version_info[0] == 3:
        if (readByte == b'\n' or readByte == b' '):
            clip_parameter = buffer
            buffer = b""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer
    else:
        if (readByte == '\n' or readByte == ' '):
            clip_parameter = buffer
            buffer = ""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer


def read_y4m_header(clip):
    if sys.version_info[0] == 3:
        header_delimiters = {b"W": 'width', b"H": 'height', b"F": 'frame_ratio', b"I": 'interlacing', b"A": 'pixel_aspect_ratio', b"C": 'bitdepth'}
    else:
        header_delimiters = {"W": 'width', "H": 'height', "F": 'frame_ratio', "I": 'interlacing', "A": 'pixel_aspect_ratio', "C": 'bitdepth'}

    y4m_params = {'width': -1,
                  'height': -1,
                  'frame_ratio': -1,
                  'framerate': -1,
                  'number_of_frames': 1,
                  'bitdepth': -1
                  }

    with open(clip, "rb") as f:
        f.seek(10)

        if sys.version_info[0] == 3:
            buffer = b""
        else:
            buffer = ""

        while True:
            readByte = f.read(1)
            if (readByte in header_delimiters.keys()):
                y4m_key = readByte
                while True:
                    readByte = f.read(1)

                    '''Use helper function to interpret byte'''
                    y4m_params[header_delimiters[y4m_key]], buffer = read_y4m_header_helper(readByte, buffer)

                    if y4m_params[header_delimiters[y4m_key]] != -1:
                        break

            if sys.version_info[0] == 3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break

        if sys.version_info[0] == 3:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(b":")
            if b'10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'
        else:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(":")
            if '10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'

        y4m_params['framerate'] = float(frame_ratio_pieces[0]) / float(frame_ratio_pieces[1])

        while f.tell() < os.path.getsize(clip):
            readByte = f.read(1)
            if binascii.hexlify(readByte) == b'0a':
                f.seek(frame_length, 1)
                buff = binascii.hexlify(f.read(5))
                if buff == b'4652414d45':
                    y4m_params['number_of_frames'] += 1

    return int(y4m_params['width']), int(y4m_params['height']), y4m_params['framerate'], y4m_params['number_of_frames']


'''functions to get clip info'''


def get_fps(clipdir, clip):
    if(".yuv" in clip and yuv_library_found):
        seq_table_index = get_seq_table_loc(seq_list, clip)
        if seq_table_index < 0:
            return 0
        fps = float(seq_list[seq_table_index]["fps_num"]) / seq_list[seq_table_index]["fps_denom"]
        return fps
    elif(".y4m" in clip):
        _, _, framerate, number_of_frames = read_y4m_header(os.path.join(clipdir, clip))
        return framerate
    else:
        return 0


def get_seq_table_loc(seq_table, clip_name):
    for i in range(len(seq_table)):
        if seq_table[i]["name"] == clip_name[:-4]:
            return i
    return -1


def get_YUV_PARAMS(clip):
    clip_name = os.path.split(clip)[1]
    seq_table_index = get_seq_table_loc(seq_list, clip_name)

    bitdepth = seq_list[seq_table_index]['bitdepth']
    ten_bit_format = seq_list[seq_table_index]['unpacked']
    width = seq_list[seq_table_index]['width']
    height = seq_list[seq_table_index]['height']
    width_x_height = '%sx%s' % (width, height)
    fps_num = seq_list[seq_table_index]['fps_num']
    fps_denom = seq_list[seq_table_index]['fps_denom']

    if (bitdepth == 8):
        number_of_frames = (int)(os.path.getsize(clip) / (width * height + (width * height / 2)))
        pixel_format = 'yuv420p'
        vvenc_pixel_format = 'yuv420'
    elif (bitdepth == 10):
        pixel_format = 'yuv420p10le'
        vvenc_pixel_format = 'yuv42010'  # Not sure what the real one is
        if ten_bit_format == 2:
            number_of_frames = (int)(((float)(os.path.getsize(clip)) / (width * height + (width * height / 2))) / 1.25)
        else:
            number_of_frames = (int)(((os.path.getsize(clip)) / (width * height + (width * height / 2))) / 2)

    return width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, '%s/%s' % (fps_num, fps_denom), float(float(fps_num) / float(fps_denom)), vvenc_pixel_format, pixel_format


def sort_clip_list_by_complexity(input_folder):
    files = glob.glob('%s/*' % input_folder)

    '''For the case where the key being sorted are identical, perform sub-sorts base on the preceding sorts.'''
    files.sort(key=lambda f: get_fps(input_folder, f), reverse=True)
    files.sort(key=lambda f: f.lower())
    files.sort(key=lambda f: (os.stat(os.path.join(input_folder, f)).st_size), reverse=True)
    Y4M_HEADER_SIZE = 80

    '''Group sorted clips into nested lists based on filesize'''
    clip_lists = []
    clip_list = []
    size_0 = os.stat(os.path.join(input_folder, files[0])).st_size
    for file_ in files:
        if (file_.endswith('yuv') or file_.endswith('y4m')):
            if file_.endswith('yuv') and os.stat(os.path.join(input_folder, file_)).st_size == size_0:
                clip_list.append(file_)
            elif file_.endswith('y4m') and size_0 - Y4M_HEADER_SIZE <= os.stat(os.path.join(input_folder, file_)).st_size <= size_0 + Y4M_HEADER_SIZE:
                clip_list.append(file_)
            else:
                clip_lists.append(clip_list)
                clip_list = []
                size_0 = os.stat(os.path.join(input_folder, file_)).st_size
                clip_list.append(file_)
    clip_lists.append(clip_list)

    return clip_lists


def test_script(unit_test):
    TEST_REPOSITORY = test_configurations()

    for feature_to_test in unit_test:
        TEST_SETTINGS['test_config'] = feature_to_test
        generate_test(feature_to_test)

        test_dir = os.path.join(os.getcwd(), 'test')
        config_dir = os.path.join(test_dir, feature_to_test)

        '''Make folders for the unit tests'''
        if not os.path.isdir(test_dir):
            os.mkdir(test_dir)
        if not os.path.isdir(config_dir):
            os.mkdir(config_dir)

        cwd_files = glob.glob('%s/*' % os.getcwd())
        file_to_execute = ''

        for cwd_file in cwd_files:
            file_name = os.path.split(cwd_file)[-1]

            if 'test' in file_name or 'old' in file_name or 'SPIE2021' in file_name:
                continue

            if file_name not in ['PyCollectResults.py', 'PyGenerateCommands.py', 'tools', 'test', 'encoders']:
                shutil.move(cwd_file, os.path.join(config_dir, file_name))
                if 'all-paral.sh' in file_name:
                    file_to_execute = os.path.join(config_dir, file_name)
            else:
                try:
                    if 'encoders' in file_name:
                        files = os.listdir(cwd_file)
                        for fname in files:
                            shutil.copy2(os.path.join(cwd_file, fname), config_dir)
                    else:
                        shutil.copytree(cwd_file, os.path.join(config_dir, file_name))

                except Exception as e:  # python >2.5
                    print(e)
                    shutil.copy(cwd_file, os.path.join(config_dir, file_name))

        shellscript = subprocess.Popen([file_to_execute], cwd=config_dir, stdin=subprocess.PIPE)
        shellscript.wait()


def check_script_integrity():
    TEST_REPOSITORY = test_configurations()
    config_hashes = dict()
    overwrite_reference_hashes = False

    '''Backup original parameter configuration'''
    backup_presets = TEST_SETTINGS['presets']
    backup_intraperiod = TEST_SETTINGS['intraperiod']
    backup_decode_cycles = TEST_SETTINGS['decode_cycles']
    backup_decode_threads = TEST_SETTINGS['decode_threads']
    backup_decode_jobs = TEST_SETTINGS['decode_jobs']
    backup_test_config = TEST_SETTINGS['test_config']

    '''Default test configuration for integrity check'''
    TEST_SETTINGS['presets'] = [13]
    TEST_SETTINGS['intraperiod'] = -1
    TEST_SETTINGS['decode_cycles'] = 1
    TEST_SETTINGS['decode_threads'] = 3
    TEST_SETTINGS['decode_jobs'] = 20

    for test_config in TEST_REPOSITORY:
        TEST_SETTINGS['test_config'] = test_config

        encoding_commands, metric_commands, copy_commands, downscale_commands, decode_commands = generate_test(test_config, True)

        encoding_commands_hash = hashlib.md5(str(encoding_commands).encode()).hexdigest()
        metric_commands_hash = hashlib.md5(str(metric_commands).encode()).hexdigest()
        copy_commands_hash = hashlib.md5(str(copy_commands).encode()).hexdigest()
        downscale_commands_hash = hashlib.md5(str(downscale_commands).encode()).hexdigest()
        decode_commands_hash = hashlib.md5(str(decode_commands).encode()).hexdigest()

        config_hashes[test_config] = [encoding_commands_hash, metric_commands_hash, copy_commands_hash, downscale_commands_hash, decode_commands_hash]

    '''Restore original test parameters'''
    TEST_SETTINGS['presets'] = backup_presets
    TEST_SETTINGS['intraperiod'] = backup_intraperiod
    TEST_SETTINGS['decode_cycles'] = backup_decode_cycles
    TEST_SETTINGS['decode_threads'] = backup_decode_threads
    TEST_SETTINGS['decode_jobs'] = backup_decode_jobs
    TEST_SETTINGS['test_config'] = backup_test_config

    '''Get previous hashes from script'''
    with open(os.path.basename(__file__)) as current_script:
        script_content = current_script.read()
        reference_hashes = script_content.split('CONFIG_HASHES = ')[-1]

    mod_hashes = ast.literal_eval(str(config_hashes).strip())
    ref_hashes = ast.literal_eval(str(reference_hashes).strip())

    if mod_hashes == ref_hashes:
        print('[PASSED] Command verification successful')
    else:
        for mod_key, ref_key in zip(mod_hashes, ref_hashes):
            if mod_hashes[mod_key] != ref_hashes[ref_key]:
                print('[CRITICAL] %s configuration has deviated from the reference' % mod_key)
                overwrite_reference_hashes = input('Would you like overwrite the reference command hashes? (y/n) -> ')

        if overwrite_reference_hashes == 'y':
            with open(os.path.basename(__file__), 'w') as current_script:
                script_content = script_content.replace(reference_hashes, str(config_hashes))
                current_script.write(script_content)
            print('References hashes have been updated. Please reopen the script to load the changes')
            sys.exit()
        else:
            print('Please check the parameters for the conflicting test configurations and retry. Exiting...')
            sys.exit()

def check_if_latest_version():
    try:
        # For Python 3.0 and later
        from urllib.request import urlopen
    except ImportError:
        # Fall back to Python 2's urllib2
        from urllib2 import urlopen
    
    start = r'# Changeable Settings Start'
    end = r'# Changeable Settings End'
    git_url = 'https://gitlab.com/AOMediaCodec/aom-testing/-/raw/master/svt-av1-testing-scripts/Upgrade_scripts_WIP/PyGenerateCommands.py'
    response = urlopen(git_url)
    reference_script = str(response.read())

    with open(os.path.basename(__file__)) as current_script:
        script_content = current_script.read()
        mod_upper = script_content.split(start)[0]
        mod_lower = script_content.split(end)[-1]
        
    ref_upper = reference_script.split(start)[0]
    ref_lower = reference_script.split(end)[-1]
        
    if mod_upper == ref_upper and mod_lower == ref_lower:
        print("[PASS] Script is the latest version")
    else:
        print("[WARNING] There is a newer version available that may contain important bug fixes")


if __name__ == '__main__':
    '''Import the YUV Library to assign parameters for YUV clips which have no embedded meta data'''
    try:
        from yuv_library import getyuvlist
        seq_list = getyuvlist()
        yuv_library_found = 1
    except ImportError:
        print("WARNING yuv_library not found, only generating commands for y4m files.")
        seq_list = []
        yuv_library_found = 0

    '''Python 2 input() doesnt accept strings, replace with raw_input for python 2 case'''
    try:
        input = raw_input
    except NameError:
        pass


    YUV_PARAMS = ['width', 'height', 'ref_width', 'mod_width', 'ref_height', 'mod_height', 'bitdepth', 'fps_num', 'fps_denom', 'fps', 'vvenc_pixfmt', 'rawvideo', 'pixfmt', 'vmaf_pixfmt', 'frames']

    RESOLUTION = {
        'netflix': [(2560, 1440), (1920, 1080), (1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216), (2560, 1088), (1920, 816), (1280, 544), (960, 408), (748, 318), (588, 250), (480, 204), (372, 158)],

        'SPIE2020_8bit': [(1280, 720), (960, 540), (640, 360), (480, 270)],
        'SPIE2020_10bit': [(1280, 720), (960, 540), (768, 432), (608, 342), (480, 270), (384, 216)],
        'SPIE2021_8bit': [(1280, 720), (960, 540), (768, 432), (640, 360), (512, 288), (384, 216), (256, 144)],
        'fast_testing_resolutions': [(384, 216), (256, 144)],

    }

    RC_VALUES = {
        'SPIE2020_x264_x265': [14, 18, 22, 27, 32, 37, 42, 47, 51],
        'SPIE2020_svt_aom': [20, 26, 32, 37, 43, 48, 55, 59, 63],

        'SPIE2021_x264_x265': [19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 41],
        'SPIE2021_svt_aom': [23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63],

        'netflix_crf': [16, 20, 24, 28, 32, 36, 39, 43, 47, 51, 55, 59, 63],
        'generic_tbr': [5000, 4000, 3000, 2000],
        'fast_testing_qps': [50, 51],
    }

    DOWNSCALE_COMMAND = {
        'SPIE2021_scaling': "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags lanczos+accurate_rnd+full_chroma_int -sws_dither none -param0 5  -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {scaled_clip_name}",
        'SPIE2020_scaling': "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags lanczos+accurate_rnd+print_info -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {scaled_clip_name}",
    }

    METRIC_COMMAND = {

        'SPIE2020_ffmpeg_psnr_ssim': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s   {width}x{height}         -r 25  -i {clip}     -lavfi "ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr" -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_vmaf': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s   {width}x{height}         -r 25  -i {clip}     -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf|version=vmaf_v0.6.1neg\\:name=vmaf_neg:feature=name=psnr\\:reduced_hbd_peak=true\\:enable_apsnr=true\\:min_sse=0.5|name=float_ssim\\:enable_db=true\\:clip_db=true:log_path={output_filename}.xml:log_fmt=xml' -threads 1 -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_psnr_ssim_vmaf': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height}         -r 25  -i {clip}     -lavfi 'ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr;[0:v][1:v]libvmaf=model_path=tools/model/vmaf_v0.6.1.pkl:log_path={output_filename}.vmaf' -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2020_ffmpeg_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+print_info [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; [scaled1][1:v]ssim=stats_file={output_filename}.ssim;[scaled2][1:v]psnr=stats_file={output_filename}.psnr;[scaled3][1:v]libvmaf=model_path=tools/model/vmaf_v0.6.1.pkl:log_path={output_filename}.vmaf'  -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',

        'SPIE2021_ffmpeg_vmaf': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {width}x{height}         -r 25  -i {clip}     -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null - > {output_filename}.log 2>&1''',
        'SPIE2021_ffmpeg_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',
        'SPIE2021_ffmpeg_vmaf_exe_rescale': r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -r 25  -i {ref_clip} -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref]" -map "[ref]" -f null - -map "[scaled]" -strict -1 -pix_fmt {pixfmt} {temp_clip} && tools/vmaf --reference {ref_clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --aom_ctc v1.0 && rm {temp_clip})> {output_filename}.log 2>&1''',

        'SPIE2021_vvenc_ffmpeg_vmaf_exe': r'''(./vvdecapp -b {output_filename}.bin -o {temp_clip} -t 1 && tools/vmaf --reference {clip} --distorted {temp_clip} -w {width} -h {height} -p 420{vmaf_pixfmt} --aom_ctc v1.0 -b {bitdepth} -o {output_filename}.xml && rm {temp_clip}) > {output_filename}.log 2>&1 ''',

        'netflix_ffmpeg_vmaf_exe_rescale': r'''(tools/ffmpeg -y -r 25 -i {output_filename}.bin -s:v {ref_width}x{ref_height}  -pix_fmt {pixfmt} -f {rawvideo} -r 25 -i {clip}  -lavfi "scale2ref=flags=lanczos+accurate_rnd+print_info:threads=1 [scaled][ref];[scaled] split=2 [scaled1][scaled2]; [scaled1][1:v]psnr=stats_file={output_filename}.psnr" -map "[ref]" -f null - -map "[scaled2]" -strict -1 -pix_fmt {pixfmt} -f {rawvideo} {temp_clip} && tools/vmaf --reference {clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --model path=./tools/model/vmaf_4k_v0.6.1.json:name=vmaf --feature psnr --threads 19 && tools/vmaf --reference {clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml_neg --model path=./tools/model/vmaf_4k_v0.6.1.json:vif.vif_enhn_gain_limit=1.0:adm.adm_enhn_gain_limit=1.0 --feature psnr --threads 19  && rm {temp_clip}) > {output_filename}.log 2>&1''',
    }

    ENCODE_COMMAND = {
        # '''SPIE2020'''
        'SPIE2020_svt_CRF_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp -enc-mode {preset} -q {rc_value} -intra-period {intraperiod} -enable-tpl-la 1 -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'SPIE2020_aom_CRF_2p': '''(/usr/bin/time --verbose   ./aomenc  --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --passes=2 --verbose  --lag-in-frames=25 --auto-alt-ref=1 --end-usage=q  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',

        'SPIE2020_x264_CRF_1p': '''(/usr/bin/time --verbose   ./x264  --preset {preset} --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --threads 1  --tune psnr  --stats {output_filename}.stat   -o {output_filename}.bin  {clip})  > {output_filename}.txt 2>&1 ''',

        'SPIE2020_x265_CRF_1p': '''(/usr/bin/time --verbose   ./x265  --preset {preset} --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth} --frame-threads 1 --no-wpp  --tune  psnr  --stats {output_filename}.stat  {clip}  -o {output_filename}.bin)  > {output_filename}.txt 2>&1 ''',

        'SPIE2020_vp9_CRF_2p': '''(/usr/bin/time --verbose   ./vpxenc  --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 --auto-alt-ref=6 --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',


        # '''SPIE2021'''
        'SPIE2021_svt_CRF_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1  --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'SPIE2021_aom_CRF_2p': '''(/usr/bin/time --verbose   ./aomenc --cpu-used={preset} --cq-level={rc_value} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --passes=2 --verbose  --lag-in-frames=35 --auto-alt-ref=1 --end-usage=q  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom}  -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',

        'SPIE2021_x265_CRF_1p': '''(/usr/bin/time --verbose   ./x265  --preset {preset} --crf {rc_value} --keyint {intraperiod}  --min-keyint {intraperiod} --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --tune  psnr  --stats {output_filename}.stat  --pools 1  --no-scenecut   --no-wpp   {clip}  -o {output_filename}.bin)  > {output_filename}.txt 2>&1 ''',

        'SPIE2021_x264_CRF_1p': '''(/usr/bin/time --verbose   ./x264  --preset {preset}  --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --threads 1  --tune psnr  --stats {output_filename}.stat  --no-scenecut   -o {output_filename}.bin  {clip})  > {output_filename}.txt 2>&1 ''',

        'SPIE2021_vvenc_CRF_1p': '''(/usr/bin/time --verbose   ./vvencapp  --input {clip} --preset {vvenc_preset}  --qp {rc_value} --intraperiod {intraperiod} --size {width}x{height}  --format  {vvenc_pixfmt}  --internal-bitdepth {bitdepth}  --framerate {fps}  --threads 1  --output {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'SPIE2021_vp9_CRF_2p': '''(/usr/bin/time --verbose   ./vpxenc --ivf --codec=vp9  --tile-columns=0 --arnr-maxframes=7 --arnr-strength=5 --aq-mode=0 --bias-pct=100 \
            --minsection-pct=1 --maxsection-pct=10000 --i420 --min-q=0 --frame-parallel=0 --min-gf-interval=4 --max-gf-interval=16 --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 \
            --auto-alt-ref=6  --threads=1  --profile=0  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --fps={fps_num}/{fps_denom} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --cq-level={rc_value} --cpu-used={preset} -o  {output_filename}.bin    {clip})  > {output_filename}.txt 2>&1''',

        #'''Default commands'''
        'svt_CRF_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1 --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

        'svt_CRF_1lp_2p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1 --passes 2 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

        'svt_CRF_nonlp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'svt_CRF_nonlp_2p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -q {rc_value}    --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --passes 2 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'svt_VBR_1lp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} --tbr {rc_value} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 1 --rc 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

        'svt_VBR_1lp_2p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} --tbr {rc_value} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 2 --rc 1 --irefresh-type 2  -i  {clip} --stats {output_filename}.stat -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',


        'ffmpeg_svt_fast_decode': '''(/usr/bin/time --verbose ./ffmpeg -y  -s:v {width}x{height}  -pix_fmt {pixfmt}  -r {fps} -f {rawvideo}  -i {clip}  -crf {rc_value}  -preset {preset}  -g {intraperiod}  -threads 1  -c:v libsvtav1  -f ivf   -svtav1-params lp=1:fast-decode=1  {output_filename}.bin ) > {output_filename}.txt 2>&1'''
    }

    main()


CONFIG_HASHES = {'SPIE2021_svt_for_testing': ['6dff9dc24a9929f6e56f9039866d1f9e', '740c8e3e09f2d541ada4af0ea6bc823f', 'f823b37db928adfba1975a01f74b3ada', '74579d16c86d19073c53e9e427f2bcf0', '0b4a343ac3e14ebe69d63d274ac87538'], 'SPIE2021_svt': ['d900598f58cb1bc637b1e5b95147dbd5', 'e220b10a3534812fa07773386ce91b31', 'f823b37db928adfba1975a01f74b3ada', '46b976ac3f45ddef206cec390603c15e', 'a650405af229e724ce2d05854c08eefa'], 'SPIE2021_aom': ['7f549d3ca3856be14d0ac286c3fabe48', 'd2aaef712801d00c7192e85f8eb7f201', 'f823b37db928adfba1975a01f74b3ada', '46b976ac3f45ddef206cec390603c15e', 'af1a4143732beca745e0a8402b34cae1'], 'SPIE2021_x264': ['017770e0695b296db424462098502dd8', '7aa024dbd36d3e54898247bc758df486', 'f823b37db928adfba1975a01f74b3ada', '46b976ac3f45ddef206cec390603c15e', '8b5c2ec94d17f7e630b76e516ded71e5'], 'SPIE2021_x265': ['5407fa13c83277428131d553926b8942', '3888c2111a56cca16fdb8585bd7cbd2a', 'f823b37db928adfba1975a01f74b3ada', '46b976ac3f45ddef206cec390603c15e', 'b82de05911c0ae71458709876e5652d7'], 'SPIE2021_vp9': ['1b4b139d49befbf165208f53119e515c', '3357b4352ab0392f8816886fd1e86e42', 'f823b37db928adfba1975a01f74b3ada', '46b976ac3f45ddef206cec390603c15e', '9d0654071ce23f4bc8f2897dd6acb59e'], 'SPIE2020_8bit_svt': ['6aab3e6edc11a5d6f80455da3285ec25', 'c43d964188ea0fc276bf0f2f0a4f16ca', 'f823b37db928adfba1975a01f74b3ada', '49bdd0505bd563602126ca35d9f9803c', '674cce78a78df97356f573566b952ddb'], 'SPIE2020_8bit_aom': ['86aeb63edf97cb60ac902c4685b2260c', '05f71d7fd4ee1a964e38f9d197d96ceb', 'f823b37db928adfba1975a01f74b3ada', '49bdd0505bd563602126ca35d9f9803c', '3ca27aa45f9ed1579040e73833c8209b'], 'SPIE2020_8bit_x264': ['702015ee349bb185581cd849c27b29e9', '319a7eb4a13b7da84cb3f94207bad0df', 'f823b37db928adfba1975a01f74b3ada', '49bdd0505bd563602126ca35d9f9803c', 'ac3e395b710cd3c0e14b4a308acc84ed'], 'SPIE2020_8bit_x265': ['c96be587b5ecdff893695297158e560a', '2618dad0eee621dea4d3b0516e2d2e4c', 'f823b37db928adfba1975a01f74b3ada', '49bdd0505bd563602126ca35d9f9803c', '7a5b456c5eccc225af81393551d78782'], 'SPIE2020_8bit_vp9': ['daf5143d520f7c4a85949fdb5802ed0c', '94abe8e18fc6741ef1e5d553283c5e7f', 'f823b37db928adfba1975a01f74b3ada', '49bdd0505bd563602126ca35d9f9803c', '905940ce32ca0f1dde319da8adbdf111'], 'SPIE2020_10bit_svt': ['0cbcf1bcc1cfbe93f2193c6cb87bd0dd', '389490d66a79c1e0549282c5a08e050d', 'f823b37db928adfba1975a01f74b3ada', '9fe7153eac3cfb24e0ca4965e3543dd0', '42e58e022828c9f3a32c1cbd01b9b961'], 'SPIE2020_10bit_aom': ['e58319c469c579b5f0e0a9e44dcb3e3b', '583a1edff05df0e9b62265a6945d4f78', 'f823b37db928adfba1975a01f74b3ada', '9fe7153eac3cfb24e0ca4965e3543dd0', '4a2ba23e3977211332c7f1d4cd7889eb'], 'SPIE2020_10bit_x264': ['9c0b9874bc8b7508738cf2ffdb29fb87', 'ed08cab8cbbd793d71f318bdcb4adf1c', 'f823b37db928adfba1975a01f74b3ada', '9fe7153eac3cfb24e0ca4965e3543dd0', 'f2a38d3b7987114545710306a6ce12d4'], 'SPIE2020_10bit_x265': ['6a261f96e4da141ce8e8571821c1efc3', '765060da07e79ef3fbf6ea40f6b88553', 'f823b37db928adfba1975a01f74b3ada', '9fe7153eac3cfb24e0ca4965e3543dd0', 'deff99721bd5566b541f4895ce9cb566'], 'SPIE2020_10bit_vp9': ['8c458cbdbbe4da3559b978edb080516e', 'fe0074f2b54b60b22a56c7feca3054cf', 'f823b37db928adfba1975a01f74b3ada', '9fe7153eac3cfb24e0ca4965e3543dd0', '016f40b0c223ab03f66e3e9e27fa325b'], 'ELFUENTE_ffmpeg_svt_fast_decode': ['fde74e988918be343328cd9a92c7622e', '1b6effc3ede7dfc5e6457e3983c7a5ff', 'f823b37db928adfba1975a01f74b3ada', 'd9f5b8a6ef2458117b4b1e2e0d350e15', '5f0921b8aeca6109bd73f4984c9a79cd'], 'NETFLIX_svt': ['1f6ee6836ef5d19609b1c75a8d2cc7cf', 'b18e5da5b7451931a12865c948144986', 'f823b37db928adfba1975a01f74b3ada', 'cc3048c38b782df72c5411a4f4fb93d3', '4770cd8f855de7c15dac6600be976d0f'], 'NETFLIX_libaom': ['b65ff6f8afaf1a06046b8c5b8547f10b', 'd63dcdae7a55406dbb8e9bf28ad42e6d', 'f823b37db928adfba1975a01f74b3ada', 'cc3048c38b782df72c5411a4f4fb93d3', 'dd1205f3e47e4d162fc27ea69924c4ce']}
